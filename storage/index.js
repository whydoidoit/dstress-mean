
var mongoose = require('mongoose');
var async = require('async');
var Q = require('q');
mongoose.connect("mongodb://54.255.237.128/data");
var db = mongoose.connection;
db.once('open', function() {
    console.log('yay! database open');
});



var queue = async.queue(function(task, callback) {
    DataBlock.findOne({ type: task.type, id: task.id}, handle(task.write, function (block) {

        var item = block || new DataBlock();

        item.data = item.data || {};
        var data = task.data || {};
        item.type = task.type;
	    item.author = task.author;
        item.id = task.id;
        if (block) {
            if(task.revision != item.revision) {
                console.log("Bad revision");
                var result = item.toObject();
                result.$notwritten$ = true;
                callback(null);
                return result;
            }
            var predicate = task.predicate || {};
            for (var i in predicate) {
                if (predicate.hasOwnProperty(i) && item.data[i] !== undefined && predicate[i] !== '') {
                    if (predicate[i] != item.data[i]) {
                        console.log("Incorrect Predicate " + i);
                        var result = item.toObject();
                        result.$notwritten$ = true;
                        callback(null);
                        return result;
                    }

                }
            }
        }
        var newData = {};

        for (var i in data) {
            newData[i] = data.hasOwnProperty(i) ? data[i] : item.data[i];
        }
        item.data = newData;
        item.lastUpdateTime = Date.now();
        item.revision++;
        item.save(function (err) {
            callback(null);
        });
        return item.toObject();
    }));
});


var Schema = mongoose.Schema;

var dataBlockSchema = module.exports.dataBlockSchema = new Schema({
    type: String,
    id: String,
    revision: { type: Number, default: 0 },
	author: { type: String, default: ""},
    lastUpdateTime: { type: Date, default: Date.now },
    data: {}
});

var dataStructureSchema = module.exports.dataStructureSchema = new Schema({
	type: String,
	structure: {}
});

dataStructureSchema.index({type:1});

dataBlockSchema.index({id: 1});
dataBlockSchema.index({type: 1, id: 1});
dataBlockSchema.index({lastUpdateTime: 1});

var DataBlock = module.exports.DataBlock = mongoose.model('Storage4', dataBlockSchema);
var DataStructure = module.exports.DataStructure = mongoose.model('Structure', dataStructureSchema);

function createResult(time, data) {
    return { updateTime: time, data: data, error: data.error };
}

function process(fn) {

    return function(req, res) {
        try {
            function writeResult(data) {
	            var time = Date.now();
	            res.setHeader('Content-Type', 'application/json');
                var result = data || {};
                res.end(JSON.stringify(createResult(time, result)));
            }

            fn(req, writeResult, res);
        } catch(e) {
            console.log(e);
            res.end(JSON.stringify(createResult(Date.now(), {error: 'exception', exception: JSON.stringify(e)})));
        }
    };
}

function getId(req) {
    return {'type': req.params.type, 'id': req.params.id };
}

function convertToNumber(something) {
	if(!something) return 0;
	var points = 0;
	try {
		points = parseInt(something);
	} catch(e) {
	}
	if(isNaN(points)) points = 0;
	return points;
}


function handle(write, fn, callback) {
    return function(err, data) {
        if(err) {
            write( { error: err } );
        }
		if(data && data.type) {
			//Ensure the data structure of the data element
			DataStructure.findOne({type: data.type}, function (err, structure) {
				if (!err && structure) {
					verifyStructure(data.data, structure.structure);
				}
				write(fn ? fn(data) : data);
				if (callback) callback();
			});
		} else {
			write(fn ? fn(data) : data);
			if (callback) callback();
		}

    };

}


var lastCached;
var cachedResult;

module.exports.route = {
	addstructure: function(req, res) {
		DataStructure.findOne({type: req.params.type}, function(err, data) {
			if(!err) {
				if (!data) {
					data = new DataStructure();
					data.type = req.params.type;
					data.structure = {};
				}
				if (data) {
					var newData = {};
					data.structure = JSON.parse(JSON.stringify(data.structure));
					for(var x in data.structure) {
						if(data.structure.hasOwnProperty(x)) {
							newData[x] = data.structure[x];
						}
					}
					for (var i in req.body.structure) {
						if (req.body.structure.hasOwnProperty(i)) {
							newData[i] = req.body.structure[i];
						}
					}
					data.structure = newData;
				}
				data.save(function(err) {
					if(err)
						console.error(err);
				});
			}
			res.end();
		});
	},
	
	author: process(function(req,write) {
		DataBlock.find({id: req.params.id, author: { $ne: req.params.author } }, handle(write));
	}),
    readall: process(function(req,write) {
        DataBlock.find({id: req.params.id }, handle(write));
    }),
    updated: process(function(req,write) {
        DataBlock.find({ id: req.params.id, lastUpdateTime: { $gt: ((new Date(+req.params.time )))} }, handle(write));
    }),
    read: process(function (req,write) {
            DataBlock.find(getId(req), handle(write));
        }),
    write: process(function(req,write) {
    	console.log(req.body ? "body is present" : "didn't find a body");
        console.log("process request " + req.params.type + " " + req.body.revision);
        queue.push({id: req.params.id, type: req.params.type, author: req.params.author, write: write, data: req.body.data, predicate: req.body.predicate, revision: req.body.revision});
    })

};


