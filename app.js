var express = require('express');
var http = require('http');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var storage = require('./storage');
var lessMiddleware = require('less-middleware');

var routes = require('./routes');
var users = require('./routes/user');

var app = express();
app.use(function (req, res, next) {

	// Website you wish to allow to connect asd
	res.setHeader('Access-Control-Allow-Origin', '*');

	// Request methods you wish to allow
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	res.setHeader('Access-Control-Allow-Credentials', true);

	// Pass to next layer of middleware
	next();
});
app.configure(function() {
	app.use(function (req, res, next) {

		// Website you wish to allow to connect asd
		res.setHeader('Access-Control-Allow-Origin', '*');

		// Request methods you wish to allow
		res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

		// Request headers you wish to allow
		res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

		// Set to true if you need the website to include cookies in the requests sent
		// to the API (e.g. in case you use sessions)
		res.setHeader('Access-Control-Allow-Credentials', true);

		// Pass to next layer of middleware
		next();
	});
    app.use(lessMiddleware( path.join(__dirname, '/public'), {
        compress: true
    }));
	app.set('port', 80);
	app.set('views', path.join(__dirname, 'views'));
	app.set('view engine', 'jade');
	app.use(favicon());
	app.use(logger('dev'));
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded());
	app.use(express.bodyParser());
	app.use(cookieParser());
	app.use(app.router);
});

// view engine setup


app.use(express.static(path.join(__dirname, 'public')));

app.get('/api/data/author/:id/:user', storage.route.author);
app.get('/api/data/updated/:id/:time', storage.route.updated);
app.get('/api/data/:id', storage.route.readall);
app.get('/api/data/:id/:type', storage.route.read);
app.post('/api/data/:id/:type/:author', storage.route.write);

app.get('/serverApi/kill', function() { process.exit(); });
app.use('/components', express.static(__dirname + '/bower_components'));
app.get('/', routes.index);
app.get('/users', users.list);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.render('error', {
        message: err.message,
        error: {}
    });
});



//app.listen(110);

module.exports = app;
